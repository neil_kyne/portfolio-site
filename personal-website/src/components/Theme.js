import react, { Component } from 'react'
import { darkMode } from './PortfolioSection'

class Theme extends Component {
  constructor(props) {
    super(props)

    this.state = { darkMode }
  }
}

export default Theme;
