import React, { useState } from 'react'
import Header from './Header'
import Footer from './Footer'
import PortfolioSection from './PortfolioSection'

import { Row } from "react-materialize";
import { darkMode } from "../ProfileInformation";
const Home = () => {
    const [darkModeProp, setDarkModeProp] = useState(darkMode)
    const darkModePropToggler = () => {
      setDarkModeProp(!darkModeProp)
    }
  
    return (
      <div className="HomeSection">
        <Header
          darkMode={darkModeProp}
          onSwitch={darkModePropToggler}
        />
        <Row style={{ margin: "0" }}>
          <PortfolioSection darkMode={darkModeProp} />
        </Row>
        <Footer />
      </div>
    );
  }
  
  export default Home;